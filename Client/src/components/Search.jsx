import React,{useState} from 'react'
import { styled, alpha, ThemeProvider } from "@mui/material/styles";
import SearchIcon from "@mui/icons-material/Search";
import InputBase from "@mui/material/InputBase";
const Search = () => {
    const [searchText, setSearchText] = useState("");
    // console.log(username);
  const Search = styled("div")(({ theme }) => ({
    position: "relative",
    display: "flex",
    justifyItems: "center",
    alignItems: "center",
    width: "50%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(1),
      width: "15rem",
      border: "1px solid #ccc", // เพิ่มเส้นขอบด้วยสีเทาอ่อน
      borderRadius: "4px", // กำหนดรูปร่างให้เป็นวงกลม
       backgroundColor: "rgba(255, 255, 255, 0.8)", // กำหนดสีพื้นหลังให้โปร่งแสง
      boxShadow: "0 2px 4px rgba(0, 0, 0, 0.1)", // เพิ่มเงาบริเวณใต้กรอบ
      cursor: "pointer", // เปลี่ยนเคอร์เซอร์เป็นรูปขีดแสดงว่าเป็นสิ่งที่คลิกได้
      transition: "background-color 0.2s ease-in-out", // เพิ่มอายุการเปลี่ยนสีพื้นหลัง
      "&:hover": {
        backgroundColor: "rgba(0, 0, 0, 0.1)", // สีพื้นหลังเมื่อโฮเวอร์
      },
    },
  }));
  const SearchIconWrapper = styled("div")(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  }));
  const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: "inherit",
    "& .MuiInputBase-input": {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)})`,
      transition: theme.transitions.create("width"),
      width: "100%",
      [theme.breakpoints.up("md")]: {
        width: "20ch",
      },
    },
  }));
  return (
    <div className="search-bar">
            <Search>
              <SearchIconWrapper>
                <SearchIcon />
              </SearchIconWrapper>
              <StyledInputBase
                placeholder="Search…"
                inputProps={{ "aria-label": "search" }}
                value={searchText}
                onChange={(event) => {
                  setSearchText(event.target.value);
                }}
              />
            </Search>
          </div>
  )
}

export default Search
