import React from 'react';
import { useState } from 'react';

function Carousel() {
  const [index, setIndex] = useState(0);

  const handlePrev = () => {
    setIndex((prevIndex) => (prevIndex === 0 ? 2 : prevIndex - 1));
  };

  const handleNext = () => {
    setIndex((prevIndex) => (prevIndex === 2 ? 0 : prevIndex + 1));
  };

  return (
    <div id="carouselExample" className="carousel slide" data-bs-ride="carousel">
      <div className="carousel-inner">
        <div className={`carousel-item ${index === 0 ? 'active' : ''}`}>
          <img src="https://www.matichon.co.th/wp-content/uploads/2018/11/%E0%B8%9B%E0%B8%81-53.jpg" className="d-block w-100" alt="..."/>
        </div>
        <div className={`carousel-item ${index === 1 ? 'active' : ''}`}>
          <img src="https://www.everydaymarketing.co/wp-content/uploads/2020/12/20-Marketing-Book-Recommended-2021-part-1.jpg" className="d-block w-100" alt="..."/>
        </div>
        <div className={`carousel-item ${index === 2 ? 'active' : ''}`}>
          <img src="https://lh6.googleusercontent.com/bNOA51CGHHU1QFlHHWEAQlHVgDmVWpKkNmEfMNUr9jI9nijDktk1fIzBjU0R9HjszvOtbk_KXJujKizmjlWm5WH2pd0rwYHUInxS7pEMn0EaFzLo2-Iw4oLcKsPgHSpROS0Fgnizzlj4lNio1EzIhg" className="d-block w-100" alt="..."/>
        </div>
      </div>
      <button className="carousel-control-prev" type="button" onClick={handlePrev}>
        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
        <span className="visually-hidden">Previous</span>
      </button>
      <button className="carousel-control-next" type="button" onClick={handleNext}>
        <span className="carousel-control-next-icon" aria-hidden="true"></span>
        <span className="visually-hidden">Next</span>
      </button>
    </div>
  );
}

export default Carousel;
