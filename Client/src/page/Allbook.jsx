import React, { useState, useEffect } from "react";
import Search from "../components/Search";
import Card from "../components/Card";
import { Box, Stack } from "@mui/material";
import authHeader from "../service/auth.header";
import axios from "axios";
import Swal from "sweetalert2";

const URL = import.meta.env.VITE_BASE_URL;
const USERNAME = import.meta.env.VITE_BASE_USERNAME;
const PASSWORD = import.meta.env.VITE_BASE_PASSWORD;
const config = {
  auth: {
    username: USERNAME,
    password: PASSWORD,
  },
  headers: authHeader(),
};

const handleDelete = async (id) => {
  Swal.fire({
    title: "Are you sure?",
    text: "You won't be able to revert this!",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "Yes, delete it!",
  }).then(async (result) => {
    if (result.isConfirmed) {
      try {
        await axios.delete(`${URL}/books/${id}`, config);

        await Swal.fire("Deleted!", "Your file has been deleted.", "success");
        // สั่งรีโหลดหน้าของ page  เพื่อจะให้ useEfect ทำงานอีกครั้ง
        window.location.reload();
      } catch (error) {
        console.error(error);
      }
    }
  });
};
const Allbook = () => {
  const [books, setBooks] = useState([]);
  const [searchText, setSearchText] = useState("");
  const [filterItems, setFilterItems] = useState([]);
  const [selectCategory, setSelectCategory] = useState("all");
  const [category, setCategory] = useState([]);
  
  

  useEffect(() => {
    const fetchAllRes = async () => {
      try {
        // console.log(data)
        const res = await axios.get(`${URL}/books`, config);
        const data = await res.data;
        setBooks(data.BookList);
        // setIsLoading(false);
        setFilterItems(data.BookList);
        setCategory(["all", ...new Set(data.BookList.map((item) => item.category))]);
        // console.log(category);
        console.log();
        // console.log(setFilterItems);

        // console.log(res.data, booklist);
      } catch (err) {
        console.log(err);
      }
    };
    fetchAllRes(books);
  }, []);
  // console.log(books);
// console.log();


  const filterItem = (category) => {
    console.log(category);
    
    const filtered = category === "all" ? books : books.filter((item) => item.category === category);
    setFilterItems(filtered);
    setSelectCategory(category);
    console.log(filtered);
  };
  // console.log(type);
  const handleClick = async () => {};

  

  return (
    <div>
      <div className="page-title">หนังสือทั้งหมด</div>
      <div className="search">
        <Search />
      </div>
      <div className="container">
        {/* menu  */}
        <div className="menu">
          {category.map((category, index) => {
            return (
              <div key={index}
              onClick={() => filterItem(category)}
              className={`${
                selectCategory === category ? "active" : ""
              } px-4 py-2 rounded-full`}>
                <button>{category}</button>
              </div>
            );
          })} 
          {/* <h1>{category[0]}</h1> */}
        </div>

        {/* menu */}
        <div className="allbook">
          <Stack
            xs={12}
            sm={6}
            md={4}
            direction={"row"}
            spacing={2}
            style={{
              display: "flex",
              flexDirection: "row",
              margin: "auto",
              padding: "10px",
              flexWrap: "wrap",
              gap: "15px",
            }}
          >
            {filterItems.map((book) => {
                    // return <div>{book.id}</div>
                return (
                  <Card
                    handleClick={handleClick}
                    handleDelete={handleDelete}
                    book={book}
                    key={book.id}
                  />
                );
              })}
          </Stack>
          {/* <h2>คอนเทนท์</h2>
          <p>เนื้อหาของคอนเทนท์ที่นี่...</p> */}
        </div>
      </div>
    </div>
  );
};

export default Allbook;
