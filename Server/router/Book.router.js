const express = require("express");
const router = express.Router();
const {getAll,createBook,getBookId,editBook,deleteId} = require("../controller/Book.controller");
const {authJwt} = require("../middleware/index")

router.get("/",getAll)
router.post("/",createBook)
router.get("/:id",getBookId )
router.put("/:id",editBook)
router.delete("/:id",deleteId)



module.exports = router;
